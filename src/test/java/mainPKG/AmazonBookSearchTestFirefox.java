package mainPKG;

import mainPKG.Containers.SearchResultElement;
import mainPKG.pageObjectTemplates.AmazonBestsellersPage;
import mainPKG.pageObjectTemplates.AmazonHomePage;
import mainPKG.pageObjectTemplates.AmazonSearchResults;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.util.List;

public class AmazonBookSearchTestFirefox {
    WebDriver webDriver;
    static String baseURL = "https://www.amazon.com/";
    static String bestsellersURL = "https://www.amazon.com/Best-Sellers-Kindle-Store-Computer-Programming/zgbs/digital-text/156140011";
    static String linkSearchRequest = "https://www.amazon.com/Head-First-Java-Kathy-Sierra/dp/0596009208/ref=sr_1_3";
    static String request = "Java";
    static String category = "Books";
    static String bestsellerPageTitle = "Amazon Best Sellers";

    @BeforeTest
    private void initDriver(){
        System.setProperty("webdriver.gecko.driver","lib/geckodriver.exe");
        webDriver = new FirefoxDriver();
    }

    @Test
    public void searchForBooks(){
        try {
            webDriver.get(baseURL);
            AmazonHomePage amazonHomePage = new AmazonHomePage(webDriver);
            amazonHomePage.search(request,category);

            WebDriverWait waitForResultsPage = new WebDriverWait(webDriver, 5);
            waitForResultsPage.until(ExpectedConditions.titleContains(request));

            Assert.assertTrue(webDriver.getTitle().contains(request));

            AmazonSearchResults amazonSearchResults = new AmazonSearchResults(webDriver);
            List<SearchResultElement> searchResults = amazonSearchResults.getResults();

            webDriver.get(bestsellersURL);

            WebDriverWait waitForBestsellersPage = new WebDriverWait(webDriver, 5);
            waitForBestsellersPage.until(ExpectedConditions.titleContains(bestsellerPageTitle));
            Assert.assertTrue(webDriver.getTitle().contains(bestsellerPageTitle));

            AmazonBestsellersPage bestsellersPage = new AmazonBestsellersPage(webDriver);
            searchResults = bestsellersPage.searchForBestSellers(searchResults);

            for (SearchResultElement element:searchResults) {
                if(linkSearchRequest.indexOf(element.getItemLink()) > 0){
                    System.out.println("Links match found!");
                }
                else {
                    System.out.println("Links do not match");
                }
                element.getBookFull();
                System.out.println();
            }

        }catch (Exception ex){
            System.out.println(ex.toString());
        }finally {
            webDriver.close();
        }
    }
}
