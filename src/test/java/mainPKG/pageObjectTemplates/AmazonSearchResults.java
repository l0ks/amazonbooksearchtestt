package mainPKG.pageObjectTemplates;

import mainPKG.Containers.SearchResultElement;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.ArrayList;
import java.util.List;

public class AmazonSearchResults {
    WebDriver driver;

    @FindBy(id = "twotabsearchtextbox")
    private WebElement searchTextBox;

    @FindBy(id = "searchDropdownBox")
    private WebElement searchDropDownBox;

    @FindBy(className = "s-result-item")
    private List<WebElement> searchResultElementList;

    public AmazonSearchResults(WebDriver driver){
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    public List<SearchResultElement> getResults(){
        String titleXPath =                 ".//div/div[1]/h2";

        String authorXPathFull =            ".//div/span/div/div/div[2]/div[2]/div/div[1]/div/div/div[1]/div";

        String ratingXPath =                ".//div/div[2]/div/span[1]/span/a/i[1]/span";

        String priceDivElemXPath =          ".//div/div/div[2]/div[2]/div/div[2]/div[1]/div/div[1]";
        String priceSpanElemVer1XPath =     ".//div[2]/div/a[2]/span[1]";
        String priceSpanElemVer2XPath =     ".//div[2]/div/a/span[1]/span[1]";

        String linkXPath =                  ".//div/div[1]/h2/a";

        List<SearchResultElement> results = new ArrayList<SearchResultElement>();
        System.out.println("Elements found on page: " + searchResultElementList.size());

        for (WebElement element:searchResultElementList) {
            WebElement webElementTitle;
            WebElement webElementAuthor;
            List<WebElement> webElementListAuthor;
            WebElement webElementRating = null;
            WebElement webElementPrice;
            WebElement webElementLink;

            StringBuilder stringBufferAuthor = new StringBuilder();
            StringBuilder stringBufferPrice = new StringBuilder();

            webElementTitle = element.findElement(
                    By.xpath(titleXPath)
            );

            webElementAuthor = element.findElement(By.xpath(authorXPathFull));
            webElementListAuthor = webElementAuthor.findElements(By.xpath(".//span | .//a" ));

            for (WebElement webElementAuthorPart:webElementListAuthor) {
                if(!webElementAuthorPart.getAttribute("innerHTML").contains("|")){
                    stringBufferAuthor.append(webElementAuthorPart.getAttribute("innerHTML"));
                }
                else {
                    break;
                }
            }

            if(element.findElements(By.xpath(ratingXPath)).size() > 0){
                /* The book has rating */
                webElementRating = element.findElement(
                        By.xpath(ratingXPath)
                );
            }

            if(element.findElements(By.xpath(priceDivElemXPath)).size() > 0){
                /* Book has buy option */
                webElementPrice = element.findElement(By.xpath(priceDivElemXPath));
                if(webElementPrice.findElements(By.xpath(priceSpanElemVer1XPath)).size() > 0){
                    WebElement price = webElementPrice.findElement(By.xpath(priceSpanElemVer1XPath));
                    stringBufferPrice.append(price.getAttribute("innerHTML"));
                }
                else if(webElementPrice.findElements(By.xpath(priceSpanElemVer2XPath)).size() > 0){
                    WebElement price = webElementPrice.findElement(By.xpath(priceSpanElemVer2XPath));
                    stringBufferPrice.append(price.getAttribute("innerHTML"));
                }
            }

            webElementLink = element.findElement(
                    By.xpath(linkXPath)
            );


            String itemLink = webElementLink.getAttribute("href");
            int idStart = itemLink.indexOf(".com/") + 5;
            int idEnd = itemLink.indexOf("/dp/");
            String newstr = itemLink.substring(idStart,idEnd);


            SearchResultElement buffer = new SearchResultElement(
                    webElementTitle.getText(),
                    stringBufferAuthor.toString(),
                    !stringBufferPrice.toString().equals("") ? stringBufferPrice.toString() : "Not available",
                    (webElementRating != null) ? webElementRating.getAttribute("innerHTML") : "Not available",
                    newstr
            );
            results.add(buffer);
        }
        return results;
    }

}
