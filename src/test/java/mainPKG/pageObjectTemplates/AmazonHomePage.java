package mainPKG.pageObjectTemplates;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class AmazonHomePage {
    private WebDriver driver;

    @FindBy(id = "twotabsearchtextbox")
    private WebElement searchTextBox;

    @FindBy(id = "searchDropdownBox")
    public WebElement searchDropDownBox;

    public AmazonHomePage(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    public void search(String request){
        System.out.println("Searching for: " + request);
        searchTextBox.sendKeys(request);
        searchTextBox.submit();
    }

    public void search(String request, String category){
        System.out.printf("Searching for: %s in %s%n", request, category);
        Select dropDown = new Select(searchDropDownBox);
        String selected = dropDown.getFirstSelectedOption().getText();

        if(selected.equals(category)){
            searchTextBox.sendKeys(request);
            searchTextBox.submit();
        }

        List<WebElement> Options = dropDown.getOptions();
        for(WebElement option:Options){
            if(option.getText().equals(category)) {
                new WebDriverWait(driver, 20).until(ExpectedConditions.elementToBeClickable(option));
                option.click();
                break;
            }
        }

        searchTextBox.sendKeys(request);
        searchTextBox.submit();
    }


}
