package mainPKG.pageObjectTemplates;

import mainPKG.Containers.SearchResultElement;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

public class AmazonBestsellersPage {
    WebDriver driver;

    @FindBy(id = "twotabsearchtextbox")
    private WebElement searchTextBox;

    @FindBy(id = "searchDropdownBox")
    private WebElement searchDropDownBox;

    @FindBy(id = "zg-ordered-list")
    private WebElement bestSellersDiv;

    public AmazonBestsellersPage(WebDriver driver){
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    public List<SearchResultElement> searchForBestSellers(List<SearchResultElement> inputList){
        //
        List<WebElement> bestSellersList = bestSellersDiv.findElements(
                By.xpath(".//span/div/span/a/div")
        );
        for (WebElement bestSellerElement : bestSellersList) {
            if(bestSellerElement.getAttribute("Title") != null){
                for (SearchResultElement resultElement: inputList) {
                    if(resultElement.isBestseller()){
                        break;
                    }
                    else {
                        resultElement.checkTitleForBestseller(bestSellerElement.getAttribute("Title"));
                    }
                }
            }
        }

        return inputList;
    }

}
