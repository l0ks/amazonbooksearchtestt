package mainPKG.Containers;

public class SearchResultElement {
    private String title;
    private String author;
    private String price;
    private String rating;
    private boolean bestseller;
    private String itemLink;

    public SearchResultElement(String title, String author, String price, String rating, String itemLink){
        this.title = title;
        this.author = author;
        this.price = price;
        this.rating = rating;
        this.itemLink = itemLink;
        setBestseller(false);
    }

    public void setBestseller(boolean bestseller) {
        this.bestseller = bestseller;
    }

    public boolean isBestseller() {
        return bestseller;
    }

    public String getTitle() {
        return title;
    }

    public String getAuthor() {
        return author;
    }

    public String getPrice() {
        return price;
    }

    public String getRating() {
        return rating;
    }

    public String getItemLink() {
        return itemLink;
    }

    public void getBookFull(){
        System.out.println();
        if (isBestseller()){
            System.out.println("Amazon Bestseller!");
        }
        System.out.println("Title: " + getTitle());
        System.out.println("Author: " + getAuthor());
        System.out.println("Price: " + getPrice());
        System.out.println("Rating: " + getRating());
    }

    public void checkTitleForBestseller(String inputTitle){
        if(inputTitle.contains(title)){
            setBestseller(true);
        }
    }

}
